<?php
include 'resource/header.php';
include 'library/config.php';
include 'library/Database.php';
$db = new Database();
?>

    <section class="contentsection">
        <div class="myform">
            <?php
            if($_SERVER['REQUEST_METHOD'] == 'POST'){

                $permitted = array('jpg' , 'jpeg' , 'png' , 'gif');
                $file_Name = $_FILES['image']['name'];
                $file_Size = $_FILES['image']['size'];
                $file_Temp = $_FILES['image']['tmp_name'];
                $div = explode('.',$file_Name);
                $file_ext = strtolower(end($div));
                $unique_image = substr(md5(time()), 0 ,10).'.'.$file_ext;
                $uploaded_image = "Images/".$unique_image;
                if(empty($file_Name)){
                    echo "<span style='color: red;font-weight: bold'>Please select any image.</span>";
                }
                elseif($file_Size > 1048567){
                    echo "<span style='color: red;font-weight: bold'>Image should be less then 1MB.</span>";
                }
                elseif (in_array($file_ext , $permitted) === false){
                    echo "<span style='color: red;font-weight: bold'>you can upload only :-".implode(',' , $permitted)."</span>";
                }
                else{
                move_uploaded_file($file_Temp ,$uploaded_image);
                $query = "INSERT INTO   imagename (name) VALUES ('$uploaded_image')";
                $inserted_rows = $db->Insert($query);
                if($inserted_rows){
                    echo  "<span style='color:forestgreen;font-weight: bold'>Image Uploaded Successfully.</span>";
                }
                else{
                    echo "<span style='color: red;font-weight: bold'>Image Not Uploaded.</span>";
                }
             }
            }
            ?>

            <form action="" method="post" enctype="multipart/form-data">
                <table>
                    <tr>
                        <td>Select Image</td>
                        <td><input type="file" name="image"> </td>
                    </tr>

                    <tr>
                        <td></td>
                        <td><input type="submit" name="submit" value="Upload"></td>
                    </tr>
                </table>

            </form>



            <table width="100%">
                <tr>
                    <th width="30%">serial</th>
                    <th width="40%">Image</th>
                    <th width="30%">Action</th>
                </tr>
                <?php
                    if(isset($_GET['id'])){
                        $id = $_GET['id'];

                        $getquery = "SELECT * FROM  imagename WHERE id=$id";
                        $getImg = $db->Select($getquery);
                while ($imgdata = $getImg->fetch_assoc()) {
                        $delImg = $imgdata['name'];
                        unlink($delImg);
                }

                        $query ="DELETE FROM imagename WHERE id = $id";
                        $delImage = $db->Delete($query);
                        if(!$delImage){
                            echo "<span style='color: red;font-weight: bold'>Image Not Deleted.</span>";
                        }

                    }
                ?>
                <?php
                $querry = "SELECT * FROM  imagename";
                $getImage = $db->Select($querry);
                if($getImage){
                $i=0;
                while ($result = $getImage->fetch_assoc()) {
                    $i++;
                    ?>
                    <tr>
                        <td><?php echo $i; ?></td>
                        <td><img src="<?php echo $result['name']; ?>" height="100px" width="100px"></td>
                        <td><a href="?id=<?php echo $result['id']; ?>">Delete</a></td>
                    </tr>
                    <?php
                }}
                ?>
            </table>
 </div>
    </section>
<?php include 'resource/footer.php'?>

