<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Image Uploading System </title>
    <style>
        body{font-family: Verdana;}
        .imagesystm{
            width:900px;margin: 0 auto;background: #ddd;}
        .headerOption ,.footeroption{background: #444;color: #fff;text-align: center;padding: 20px;}
        .headerOption h2 ,.footeroption h2{margin: 0;}
        .contentsection{min-height: 450px;padding: 20px;}
        .myform{width: 500px;margin: 0 auto;padding: 10px 20px 20px;}
        input[type ="text"],input[type = "submit" ]{cursor: pointer;}
    </style>
</head>
<body>

<div class="imagesystm">
    <section class="headerOption">
        <h2>This is Image uploading system.</h2>
    </section>