<?php
/**
 * Created by PhpStorm.
 * User: Kuddus_Munsi
 * Date: 8/16/2017
 * Time: 1:49 AM
 */

class Database
{

    public $hostName =DB_HOST;
    public $dbName = DB_NAME;
    public $dbUser = DB_USER;
    public $dbPassword = DB_PASSWORD;
    public $error;
    public $link;

    public function __construct()
    {
        $this->connectDB();
    }
    private function connectDB(){

        $this->link = new mysqli($this->hostName,$this->dbUser,$this->dbPassword,$this->dbName);

        if(!$this->link){
            $this->error = "Connection is failed." .$this->link->connect_error;
        }

    }

    public function  Insert($data){

        $row_insert = $this->link->query($data) or die($this->link->error.__LINE__);
        if($row_insert){
            return $row_insert;
        }
        else{
            return false;
        }
    }
    public function  Select($data){

        $result = $this->link->query($data) or die($this->link->error.__LINE__);
        if($result->num_rows > 0){
            return $result;
        }
        else{
            return false;
        }
    }

    public function  Delete($data){

        $delete_row = $this->link->query($data) or die($this->link->error.__LINE__);
        if($delete_row){
            return $delete_row;
        }
        else{
            return false;
        }
    }

}